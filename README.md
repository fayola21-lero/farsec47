# farsec47

## Filtering and Ranking Security Bug Reports

This application is used to build prediction models for predicting security bug reports. Included here are bug reports from five different projects, code, experiments, and results.

## Data

Downloaded with script/download-data.sh and located in "resources/data/downloaded" and "resources/data/downloaded".

The data generated for the experiments are located in:

* resources/data/text-frequency/within/

* resources/data/text-frequency/transfer/

## Results

resources/data/text-frequency/results/

## Usage

[Experiments](http://viewer.gorilla-repl.org/view.html?source=bitbucket&user=fayola21-lero&repo=farsec47&path=src/farsec47/experiments.clj) and [Demos](http://viewer.gorilla-repl.org/view.html?source=bitbucket&user=fayola21-lero&repo=farsec47&path=src/farsec47/demos.clj)

or

[Run example on command line with farsec47.jar](https://bitbucket.org/fayola21-lero/farsec47/wiki/Home)



## License

Copyright © 2017

Distributed under the Eclipse Public License either version 1.0.
