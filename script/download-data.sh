#!/bin/bash

script_dir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
data_dir="${script_dir}/../resources/data/downloaded"
ambari_url="http://oss.sys.wakayama-u.ac.jp/HIB/Ambari.csv"
camel_url="http://oss.sys.wakayama-u.ac.jp/HIB/Camel.csv"
derby_url="http://oss.sys.wakayama-u.ac.jp/HIB/Derby.csv"
wicket_url="http://oss.sys.wakayama-u.ac.jp/HIB/Wicket.csv"
chromium_url="http://2011.msrconf.org/msrc2011/chrome.bugs.html.zip"

mkdir -p "${data_dir}"

echo "Downloading ${ambari_url}..."
if [ $(curl -s --head -w %{http_code} $ambari_url -o /dev/null) -eq 200 ]; then
    curl -o "${data_dir}/Ambari.csv" $ambari_url
else
    echo "Couldn't download Ambari data. Perhaps it has moved?"
fi

echo "Downloading ${camel_url}..."
if [ $(curl -s --head -w %{http_code} $camel_url -o /dev/null) -eq 200 ]; then
    curl -o "${data_dir}/Camel.csv" $camel_url
else
    echo "Couldn't download Camel data. Perhaps it has moved?"
fi

echo "Downloading ${derby_url}..."
if [ $(curl -s --head -w %{http_code} $derby_url -o /dev/null) -eq 200 ]; then
    curl -o "${data_dir}/Derby.csv" $derby_url
else
    echo "Couldn't download Derby data. Perhaps it has moved?"
fi

echo "Downloading ${wicket_url}..."
if [ $(curl -s --head -w %{http_code} $wicket_url -o /dev/null) -eq 200 ]; then
    curl -o "${data_dir}/Wicket.csv" $wicket_url
else
    echo "Couldn't download Wicket data. Perhaps it has moved?"
fi

echo "Downloading ${chromium_url}..."
if [ $(curl -s --head -w %{http_code} $chromium_url -o /dev/null) -eq 200 ]; then
    curl -o "${data_dir}/chrome.bugs.html.zip" $chromium_url
    unzip "${data_dir}/chrome.bugs.html.zip" -d "${data_dir}"
else
    echo "Couldn't download Russia data. Perhaps it has moved?"
fi