(defproject farsec47 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  
  ;:jvm-opts ["-Xmx1024M"]
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [incanter/incanter-core "1.4.1"]
                 [incanter/incanter-io "1.4.1"]
                 [net.mikera/core.matrix "0.57.0"]
                 [plotly-clj "0.1.1"]
                 [org.clojure/data.csv "0.1.2"]                             
                 [clojure-opennlp "0.3.3"]
                 [seer "0.1.2-SNAPSHOT"]  
                 [lein-gorilla "0.4.0"]
                 [org.clojure/tools.cli "0.3.5"]
                 [cc.artifice/clj-ml "0.8.5"]
                 [clj-time "0.13.0"]]
  :plugins [[lein-gorilla "0.4.0"]]
  :aot [farsec47.core]
  :main farsec47.core)




