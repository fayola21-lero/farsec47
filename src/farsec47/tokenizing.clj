;; gorilla-repl.fileformat = 1

;; @@
(ns farsec47.tokenizing
  (:use [opennlp.nlp])
  (:use [opennlp.tools.filters])
  (:require [incanter.core :as i]
            [incanter.stats :as stat]
            [incanter.io :as iio] 
            [clojure.set :as set]
            [clojure.java.io :as io]))

(defn indexed
  "Returns a lazy sequence of [index, item] pairs,
   where items come from 's' and indexes count up 
   from zero.
   (indexed '(a b c d))  =>  
   ([0 a] [1 b] [2 c] [3 d])"
  [s]
  (map vector (iterate inc 0) s))

(defn normalize [token-seq]
  (map #(.toLowerCase %) token-seq))

(defn load-stopwords [filename]
  (with-open [r (io/reader filename)]
    (set (doall (line-seq r)))))

(def is-stopword 
  (load-stopwords "stopwords/english"))

(def get-sentences 
  (make-sentence-detector "models/en-sent.bin"))

(def tokenize 
  (make-tokenizer "models/en-token.bin"))

(def detokenize 
  (make-detokenizer "models/english-detokenizer.xml"))

(def pos-tag 
  (make-pos-tagger "models/en-pos-maxent.bin"))

;---text

(defn remove-punctuation 
  "Removes all non-alphanumeric characters."
  [word]
  (clojure.string/replace word #"(?i)[^\w]+" ""))

(defn is-punctuation [word]
  "Returns nil if there is no punctuation in the token."
  (if (not (empty? (clojure.string/replace word #"(?i)[\w]+" ""))) 
    "nil" word))

(defn is-alphanumeric 
  "Returns nil if there no digits in the token."
  [word]
  (if (re-find #"\d+" word) "nil" word))

(defn is-underscore 
  "Returns nil if there no underscores in the token."
  [word]
  (if (re-find #"_" word) "nil" word))

(defn is-url 
  "Returns nil if there no backslashes in the token."
  [word]
  (if (re-find #"\\" word) "nil" word))

(defn is-unwanted 
  "Returns true if word contains digits and/or punctuation."         
  [word] 
  (->> word
       (is-punctuation)
       (is-url)
       (is-alphanumeric)
       (is-underscore)))

(defn unwanted-words
  "Removes unwanted keywords, such as urls, alphnumeric,
   underscores, punctuation, stopwords and single characters."
  [words]
  (->> words
       (remove is-stopword)
       (map is-unwanted)
       (remove #(= "nil" %))
       (remove #(< (count %) 3))))
       
 (defn scrub-text
   [xs]
   (->> xs
        (map tokenize)
        (map normalize)
       	(map unwanted-words)))
 
 (defn scrub-one
   [x]
   (->> x
        (tokenize)
        (normalize)
        (unwanted-words)))

(defn normalize-number [num small big]
  (/ (- num small) (- big small)))

(defn normalize-numbers [nums small big]
  (map #(normalize-number % small big) nums))


(defn strip-html-tags
  "Messily strip html tags from a web page"
  [string]
  (-> string
      (.replaceAll "<script .*?>.*?</script>" " ")
      (.replaceAll "<style .*?>.*?</style>" " ")
      (.replaceAll "<.*?>" " ")
      (.replaceAll "[ ]+" " ")))

(defn fetch-page
  [url]
  (let [html (.replaceAll (slurp url) "[\t\n\r]" " ")]
    (re-find #"<body.*?</body>" html)))

(defn fetch-plain-page
  [url]
  (strip-html-tags (fetch-page url)))

(defn- tag-sentences
  [sent-seq]
  (map #(pos-tag (tokenize %)) sent-seq))

(defn tag-page
  [url]
  (let [page (fetch-plain-page url)
        sentences (get-sentences page)
        sent-seq (partition-all 10 sentences)]
    (pmap tag-sentences sent-seq)))
;; @@
