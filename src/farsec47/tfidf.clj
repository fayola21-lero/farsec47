;; gorilla-repl.fileformat = 1

;; @@
(ns farsec47.tfidf
  (:require [farsec47.tokenizing :as t]
            [incanter.core :as i]
            [incanter.stats :as stat]
            [incanter.io :as iio] 
            [clojure.set :as set]
            [clojure.java.io :as io]))

(defn tf [term-freq max-freq]
  (+ 0.5 (/ (* 0.5 term-freq) max-freq)))

(defn idf [corpus term]
  (Math/log
    (/ (count corpus)
       (inc (count (filter #(contains? % term) corpus))))))

(defn get-corpus-terms [corpus]
  (->> corpus
       (map #(set (keys %)))
       (reduce set/union #{})))

(defn get-idf-cache [corpus]
  (reduce #(assoc %1 %2 (idf corpus %2)) {}
          (get-corpus-terms corpus)))

(defn tf-idf [idf-value freq max-freq]
  (* (tf freq max-freq) idf-value))

(defn tf-idf-pair [idf-cache max-freq pair]
  (let [[term freq] pair]
    [term (tf-idf (idf-cache term) freq max-freq)]))

(defn tf-idf-freqs [idf-cache freqs]
  (let [max-freq (reduce max 0 (vals freqs))]
    (->> freqs
         (map #(tf-idf-pair idf-cache max-freq %))
         (into {}))))
;; @@
