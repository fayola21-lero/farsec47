;; gorilla-repl.fileformat = 1

;; **
;;; # Command Line Usage for FARSEC
;; **

;; @@
(ns farsec47.core
  (:use (farsec47 data evaluate utils))
  (:use (seer naive-bayes random-forest logistic-regression 
              ib-k multilayer-perceptron cpdp filtering))
  (:use (incanter datasets))
  (:use [clojure.tools.cli :only (cli)])
  (:require [farsec47.tfidf :as tfidf]
            [farsec47.tokenizing :as t]
            [farsec47.crosswords :as c]
            [farsec47.score-reports :as s]
            [farsec47.filter-reports :as f]
            [farsec47.rank-reports :as r]
            [farsec47.experiments :as e]
            [farsec47.demos :as dem]
            [incanter.core :as i]
            [incanter.stats :as stat]
            [incanter.io :as iio] 
            [clojure.set :as set]
            [clojure.java.io :as io]
            [clojure.core.matrix :as m]
            [clojure.core.matrix.dataset :as d]
            [clojure.core.matrix.random :as rnd]
            [clojure.data.csv :as csv])
            
  (:use [plotly-clj.core] :reload-all)
  (:gen-class))

;(use '[gorilla-repl latex table html])

;(offline-init)
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; ## Experiments
;; **

;; **
;;; ### Number of Security Cross Words in Filtered Data
;; **

;; @@
(defn pretty-num-crosswords
  [path data-name]
  (->> (e/num-crosswords path data-name)
       (zipmap ["train" "farsecsq" "farsectwo" "farsec" 
                "clni" "clnifarsecsq" "clnifarsectwo" "clnifarsec"])))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;farsec47.core/pretty-num-crosswords</span>","value":"#'farsec47.core/pretty-num-crosswords"}
;; <=

;; **
;;; ### Predict SBRs for WPP
;; **

;; @@
(defn make-wpp-prediction
  [wpp-path data-name afilter learner]
  (let [src (str wpp-path data-name "-"afilter".csv")
        tar (str wpp-path data-name "-test.csv")
        lnr (get (into {} ml) learner)]
   (cpdp-id src tar lnr)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;farsec47.core/make-wpp-prediction</span>","value":"#'farsec47.core/make-wpp-prediction"}
;; <=

;; **
;;; ### Predict SBRs for TPP
;; **

;; @@
(defn make-tpp-prediction
  [wpp-path tpp-path src-data-name tar-data-name afilter learner]
  (let [src (str wpp-path src-data-name "-"afilter".csv")
        tar (str tpp-path tar-data-name "-" src-data-name "-test.csv")
        lnr (get (into {} ml) learner)]
   (cpdp-id src tar lnr)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;farsec47.core/make-tpp-prediction</span>","value":"#'farsec47.core/make-tpp-prediction"}
;; <=

;; **
;;; ## View Sample of Bug Reports for a Project
;; **

;; @@
(defn -main [& args]
  (let [[opts args banner]
        (cli args
          ["-h" "--help" "Show help" :flag true :default false]
          ["-e" "--experiment" "experiment type" :default "wpp"]
          ["-w" "--wpp-data-path" "Path of projects" :default wpp-data-path]
          ["-t" "--tpp-data-path" "Path of projects" :default tpp-data-path]
          ["-p" "--proj" "REQUIRED: Project name" :default "wicket"]
          ["-c" "--cproj" "Source project for cross prediction" :default "camel"]
          ["-m" "--ml" "Machine Learning algorithms" :default "naive_bayes"]
          ["-s" "--filter" "train, farsecsq, farsectwo, farsec, 
           clni, farsecsq, farsectwo, or farsec" :default "farsec"]
          ["-d" "--thres" "threshold" :default "0.75"]
          ["--id-sec-words" "identify-security-words" :flag true :default false]
          ["--num-crosswords" "num-crosswords with filtering" :flag true :default false]
          ["--wpp" "make-wpp-prediction" :flag true :default false]
          ["--tpp" "make-tpp-prediction" :flag true :default false]
          ["--spred" "score prediction" :flag true :default false]
          ["--farsec-wpp-map" "mean-average-precision" :flag true :default false]
          ["--clni-wpp-map" "mean-average-precision" :flag true :default false]
          ["--farsec-tpp-map" "mean-average-precision" :flag true :default false]
          ["--clni-tpp-map" "mean-average-precision" :flag true :default false])]  
    (when (:help opts)
      (println banner)
      (System/exit 0))
    (if (:proj opts)
      (cond
        (:id-sec-words opts) ;if-then
        (-> (e/identify-security-words 
              (:proj opts)) 
            (println))
        (:num-crosswords opts)
        (-> (pretty-num-crosswords 
              (:wpp-data-path opts) 
              (:proj opts)) 
            (clojure.pprint/pprint))
        (:wpp opts)
        (-> (make-wpp-prediction 
              (:wpp-data-path opts) 
              (:proj opts) 
              (:filter opts)
              (:ml opts))
            (clojure.pprint/pprint))
        (:tpp opts)
        (-> (make-tpp-prediction 
              (:wpp-data-path opts) 
              (:tpp-data-path opts) 
              (:cproj opts)
              (:proj opts) 
              (:filter opts)
              (:ml opts))
            (clojure.pprint/pprint))
        (:spred opts)
        (-> (e/score-res
              (:proj opts)
              (read-string (:thres opts)))
            (clojure.pprint/pprint))
        (:farsec-wpp-map opts)
        (-> (dem/get-mean-avg-prec
              (:proj opts)
              (:experiment opts)
              (:wpp-data-path opts) 
              (:wpp-data-path opts))
            (clojure.pprint/pprint))
        (:clni-wpp-map opts)
        (-> (dem/get-mean-avg-prec-clni
              (:proj opts)
              (:experiment opts)
              (:wpp-data-path opts)
              (:wpp-data-path opts))
            (clojure.pprint/pprint))
        (:farsec-tpp-map opts)
        (-> (dem/get-mean-avg-prec
              (:proj opts)
              (:experiment opts)
              (:wpp-data-path opts) 
              (:tpp-data-path opts))
            (clojure.pprint/pprint))
        (:clni-tpp-map opts)
        (-> (dem/get-mean-avg-prec-clni
              (:proj opts)
              (:experiment opts)
              (:wpp-data-path opts) 
              (:tpp-data-path opts))
            (clojure.pprint/pprint))
       banner (System/exit 0))
      (println banner))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;farsec47.core/-main</span>","value":"#'farsec47.core/-main"}
;; <=

;; @@
(symbol "naive-bayes")
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-symbol'>naive-bayes</span>","value":"naive-bayes"}
;; <=

;; @@
naive-bayes
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-unkown'>#function[seer.naive-bayes/naive-bayes]</span>","value":"#function[seer.naive-bayes/naive-bayes]"}
;; <=

;; @@
(into {} ml)
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-map'>{</span>","close":"<span class='clj-map'>}</span>","separator":", ","items":[{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-string'>&quot;naive_bayes&quot;</span>","value":"\"naive_bayes\""},{"type":"html","content":"<span class='clj-unkown'>#function[seer.naive-bayes/naive-bayes]</span>","value":"#function[seer.naive-bayes/naive-bayes]"}],"value":"[\"naive_bayes\" #function[seer.naive-bayes/naive-bayes]]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-string'>&quot;random_forest&quot;</span>","value":"\"random_forest\""},{"type":"html","content":"<span class='clj-unkown'>#function[seer.random-forest/random-forest]</span>","value":"#function[seer.random-forest/random-forest]"}],"value":"[\"random_forest\" #function[seer.random-forest/random-forest]]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-string'>&quot;logistic_regression&quot;</span>","value":"\"logistic_regression\""},{"type":"html","content":"<span class='clj-unkown'>#function[seer.logistic-regression/logistic-regression]</span>","value":"#function[seer.logistic-regression/logistic-regression]"}],"value":"[\"logistic_regression\" #function[seer.logistic-regression/logistic-regression]]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-string'>&quot;multilayer_perceptron&quot;</span>","value":"\"multilayer_perceptron\""},{"type":"html","content":"<span class='clj-unkown'>#function[seer.multilayer-perceptron/multilayer-perceptron]</span>","value":"#function[seer.multilayer-perceptron/multilayer-perceptron]"}],"value":"[\"multilayer_perceptron\" #function[seer.multilayer-perceptron/multilayer-perceptron]]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-string'>&quot;ib_k&quot;</span>","value":"\"ib_k\""},{"type":"html","content":"<span class='clj-unkown'>#function[seer.ib-k/ib-k]</span>","value":"#function[seer.ib-k/ib-k]"}],"value":"[\"ib_k\" #function[seer.ib-k/ib-k]]"}],"value":"{\"naive_bayes\" #function[seer.naive-bayes/naive-bayes], \"random_forest\" #function[seer.random-forest/random-forest], \"logistic_regression\" #function[seer.logistic-regression/logistic-regression], \"multilayer_perceptron\" #function[seer.multilayer-perceptron/multilayer-perceptron], \"ib_k\" #function[seer.ib-k/ib-k]}"}
;; <=

;; @@

;; @@
