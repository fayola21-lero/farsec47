;; gorilla-repl.fileformat = 1

;; @@
(ns farsec47.crosswords
  (:use [opennlp.nlp])
  (:use [opennlp.tools.filters])
  (:use [farsec47 data tfidf utils])
  (:use (incanter core charts stats io))
  (require [farsec47.tokenizing :as t]
           [farsec47.score-reports :as s]
    	   [clojure.java.io :as io]
           [clojure.set :as set]))

(defmulti security-words identity)

(defn top-security-words 
  [key-data]
  (let [sbr-corpus
        (->> (load-data key-data)
       		 (:rows)
             (folds 2 :id)
       		 (first)
       		 (:rows)
       		 (filter #(or
                        (= (:security %) "1")
                        (= (:security %) 1)))
       		 (map :report)
       		 (map t/scrub-one)
       		 (map frequencies))
        cache (get-idf-cache sbr-corpus)
        freqs (map #(tf-idf-freqs cache %) sbr-corpus)
        corpus-tfidf (reduce #(merge-with + %1 %2) {} 
                          freqs)
        top (->> corpus-tfidf
              (sort-by second >)
              (take 100))]
    top))

(defn top-security-words-nsbr 
  [key-data top-words]
  (let [nsbr-corpus
        (->> (load-data key-data)
       		 (:rows)
             (folds 2 :id)
       		 (first)
       		 (:rows)
       		 (filter #(or
                        (= (:security %) "0")
                        (= (:security %) 0)))
       		 (map :report)
       		 (map t/scrub-one)
       		 (map frequencies))
        cache (get-idf-cache nsbr-corpus)
        freqs (map #(tf-idf-freqs cache %) nsbr-corpus)
        corpus-tfidf (reduce #(merge-with + %1 %2) {} 
                          freqs)
        top (->> top-words
                 (map #(vector 
                         %
                         (get 
                           (into {} corpus-tfidf) 
 	                          % 0)))
                 (sort-by second >))]
    top))

(defn top-security-words-filtered 
  [key-data key-farsec top-words]
  (let [scored-words
        (->> (s/score-words key-data key-farsec top-words)
             (into {}))
        filtered-nsbr-corpus
        (->> (load-data key-data)
       		 (:rows)
             (folds 2 :id)
       		 (first)
       		 (:rows)
       		 (filter #(or
                        (= (:security %) "0")
                        (= (:security %) 0)))
       		 (map :report)
       		 (map t/scrub-one)
             (map #(vector % (s/score-report scored-words %)))
             (filter #(< (second %) 0.75))
             (map first)
       		 (map frequencies))
        cache (get-idf-cache filtered-nsbr-corpus)
        freqs (map #(tf-idf-freqs cache %) filtered-nsbr-corpus)
        corpus-tfidf (reduce #(merge-with + %1 %2) {} 
                          freqs)
        top (->> top-words
                 (map #(vector 
                         %
                         (get 
                           (into {} corpus-tfidf) 
 	                          % 0)))
                 (sort-by second >))]
    top))

; Ambari
(defmethod security-words :ambari [_]
  (top-security-words :ambari-scrubbed))
(defmethod security-words :ambari-nsbr [_]
  (top-security-words-nsbr 
    :ambari-scrubbed
    (map first (security-words :ambari))))
(defmethod security-words :ambari-farsec [_]
  (top-security-words-filtered 
    :ambari-scrubbed
    :none
    (map first (security-words :ambari))))
(defmethod security-words :ambari-farsectwo [_]
  (top-security-words-filtered 
    :ambari-scrubbed
    :two
    (map first (security-words :ambari))))
(defmethod security-words :ambari-farsecsq [_]
  (top-security-words-filtered 
    :ambari-scrubbed
    :sq
    (map first (security-words :ambari))))

; Camel
(defmethod security-words :camel [_]
  (top-security-words :camel-scrubbed))
(defmethod security-words :camel-nsbr [_]
  (top-security-words-nsbr 
    :camel-scrubbed
    (map first (security-words :camel))))
(defmethod security-words :camel-farsec [_]
  (top-security-words-filtered 
    :camel-scrubbed
    :none
    (map first (security-words :camel))))
(defmethod security-words :camel-farsectwo [_]
  (top-security-words-filtered 
    :camel-scrubbed
    :two
    (map first (security-words :camel))))
(defmethod security-words :camel-farsecsq [_]
  (top-security-words-filtered 
    :camel-scrubbed
    :sq
    (map first (security-words :camel))))

; Derby
(defmethod security-words :derby [_]
  (top-security-words :derby-scrubbed))
(defmethod security-words :derby-nsbr [_]
  (top-security-words-nsbr 
    :derby-scrubbed
    (map first (security-words :derby))))
(defmethod security-words :derby-farsec [_]
  (top-security-words-filtered 
    :derby-scrubbed
    :none
    (map first (security-words :derby))))
(defmethod security-words :derby-farsectwo [_]
  (top-security-words-filtered 
    :derby-scrubbed
    :two
    (map first (security-words :derby))))
(defmethod security-words :derby-farsecsq [_]
  (top-security-words-filtered 
    :derby-scrubbed
    :sq
    (map first (security-words :derby))))

; Wicket
(defmethod security-words :wicket [_]
  (top-security-words :wicket-scrubbed))
(defmethod security-words :wicket-nsbr [_]
  (top-security-words-nsbr 
    :wicket-scrubbed
    (map first (security-words :wicket))))
(defmethod security-words :wicket-farsec [_]
  (top-security-words-filtered 
    :wicket-scrubbed
    :none
    (map first (security-words :wicket))))
(defmethod security-words :wicket-farsectwo [_]
  (top-security-words-filtered 
    :wicket-scrubbed
    :two
    (map first (security-words :wicket))))
(defmethod security-words :wicket-farsecsq [_]
  (top-security-words-filtered 
    :wicket-scrubbed
    :sq
    (map first (security-words :wicket))))

; Chromium
(defmethod security-words :chromium [_]
  (top-security-words :chromium-scrubbed))
(defmethod security-words :chromium-nsbr [_]
  (top-security-words-nsbr 
    :chromium-scrubbed
    (map first (security-words :chromium))))
(defmethod security-words :chromium-farsec [_]
  (top-security-words-filtered 
    :chromium-scrubbed
    :none
    (map first (security-words :chromium))))
(defmethod security-words :chromium-farsectwo [_]
  (top-security-words-filtered 
    :chromium-scrubbed
    :two
    (map first (security-words :chromium))))
(defmethod security-words :chromium-farsecsq [_]
  (top-security-words-filtered 
    :chromium-scrubbed
    :sq
    (map first (security-words :chromium))))


;----------------Get tf-idf with data files-------------------

(defn clean-zeros 
  [xy]
  (filter #(not= (second %) 0) xy))

(defn words-tfidfs-sbr 
  [path filter-name data-name]
  (let [data 
        (read-as-csv (str path data-name "-" filter-name ".csv"))
        top-words
        (-> (first data) (rest) (butlast))
        sbrs
        (->> (rest data)
             (filter #(or (= (last %) 1) (= (last %) "1")))
             (transpose)
             (rest)
             (butlast)
             (transpose)
             (map #(vector top-words %1))
             (map #(vector (first %) (map read-string (second %))))
             (map transpose)
             (map #(clean-zeros %))
             (map #(into {} %))
             )
        cache (get-idf-cache sbrs)
        freqs (map #(tf-idf-freqs cache %) sbrs)
        corpus-tfidf (reduce #(merge-with + %1 %2) {} 
                          freqs)
        top (->> corpus-tfidf
              (sort-by second >))
        ]
    top
    ))

(defn words-tfidfs-nsbr 
  [path filter-name data-name]
  (let [data 
        (read-as-csv (str path data-name "-" filter-name ".csv"))
        top-words
        (-> (first data) (rest) (butlast))
        nsbrs
        (->> (rest data)
             (filter #(or (= (last %) 0) (= (last %) "0")))
             (transpose)
             (rest)
             (butlast)
             (transpose)
             (map #(vector top-words %1))
             (map #(vector (first %) (map read-string (second %))))
             (map transpose)
             (map #(clean-zeros %))
             (map #(into {} %))
             )
        cache (get-idf-cache nsbrs)
        freqs (map #(tf-idf-freqs cache %) nsbrs)
        corpus-tfidf (reduce #(merge-with + %1 %2) {} 
                          freqs)
        top (->> corpus-tfidf
              (sort-by second >))
        zero-words
        (->> (filter #(not (member? % (map first top))) top-words)
             (map #(vector % 0)))
        ]
    (concat top zero-words)
    ))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;farsec47.crosswords/words-tfidfs-nsbr</span>","value":"#'farsec47.crosswords/words-tfidfs-nsbr"}
;; <=

;; @@
;(->> (security-words :derby-nsbr)
;     ;(sort-by second >)
;     (filter #(= (second %) 0)))
;; @@

;; @@
;(->> (map first (security-words :ambari))
;     (s/score-reports first :ambari-scrubbed :sq)
;     
;     (sort)
;     (bin 10)
;     (histogram)
;     (view)
     ;(filter #(< % 0.9))
     ;(count)
;     )
;(security-words :wicket-filtered)
;; @@

;; @@
;(->> (read-as-csv (str wpp-data-path "ambari-train.csv"))
;     ;(:rows)
;     (first))
;; @@

;; @@
(defn pdecrease
  [orig coll]
  (map #(-> (- orig %)
            (/ orig)
            (* 100.0)
            (float))
       coll))

(defn scw
  [data-name]
  (loop [f ;["train" "farsecsq" "farsectwo" "farsec" "clnifarsecsq" "clnifarsectwo" "clnifarsec"]
           ["train" "clnifarsecsq"]
         res []]
    (if (empty? f)
      (pdecrease (first res) (rest res))
      (recur
        (rest f)
        (conj res
              (->> (words-tfidfs-nsbr wpp-data-path (first f) data-name)
                   (filter #(not= (second %) 0))
                   (count)
                   )
                 )))))

(defn scws
  []
  (map scw ["chromium" "wicket" "ambari" "camel" "derby"]))


;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;farsec47.crosswords/scws</span>","value":"#'farsec47.crosswords/scws"}
;; <=

;; @@
;(->> (security-words :ambari)
;     (sort-by second >))
;; @@
