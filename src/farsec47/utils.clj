;; gorilla-repl.fileformat = 1

;; @@
(ns farsec47.utils
  (:use (seer naive-bayes random-forest logistic-regression 
              ib-k multilayer-perceptron cpdp filtering))
  (require [clojure.java.io :as io]
           [clojure.data.csv :as csv]))

; Source http://writequit.org/blog/index.html%3Fp=351.html
(def user-home (System/getProperty "user.dir"))

(def header ["Source" "Target" "Filter" "Learner" 
             "TN" "TP" "FN" "FP" "err" "pd" "pf" 
             "prec" "f" "g-measure"])
(def data-path (str user-home "/resources/data/text-frequency/old/"))
(def wpp-data-path (str user-home "/resources/data/text-frequency/within/"))
(def tpp-data-path (str user-home "/resources/data/text-frequency/transfer/"))
(def results-data-path (str user-home "/resources/data/text-frequency/results/"))
(def ml [["naive_bayes" naive-bayes]
         ["random_forest" random-forest]
         ["logistic_regression" logistic-regression]
         ["multilayer_perceptron" multilayer-perceptron]
         ["ib_k" ib-k]])

(defn rd3 [coll] (nth coll 2))
(defn th4 [coll] (nth coll 3))
(defn th5 [coll] (nth coll 4))
(defn th6 [coll] (nth coll 5))

(defn member? [val lst]
  (get (set lst) val))

(defn combine-scores
  [scores]
  (let [good-prob (apply * scores)
        bad-prob  (apply * (map #(- 1 %) scores))]
    (float
      (/ good-prob
        (+ good-prob bad-prob)))))

(defn folds 
  [n-folds key-col xs]
  (let [n-items (Math/ceil (/ (count xs) n-folds))
        sort-col (sort-by key-col < xs)]
    (loop [x sort-col result []]
      (if (empty? x)
        result
        (recur
          (drop n-items x)
          (conj result 
                (hash-map 
                  :column-names [:id :report :security]
                  :rows (take n-items x))))))))

(defn bin
  "
    Given a sequence of sorted values and required number of bins (n-bins),
    and make sure the same values are in the same bins.
  "
  [n-bins xs]
  (if (apply = xs)
    (repeat (count xs) 0)
    (let [min-x   (apply min xs)
          max-x   (apply max xs)
          range-x (- max-x min-x)
          bin-fn  (fn [x]
                    (-> x
                        (- min-x)
                        (/ range-x)
                        (* n-bins)
                        (int)
                        (min (dec n-bins))))]
      (map bin-fn xs))))

(defn files-to-csv
  "
   Take the content of each file, apply stem, label remaining words as security
   or nonsecurity. Save output in a text file and use to build a model. Start
   with the first 10 files. Will need to append output file and each need file
   is added.

  "
  [filename] ())

(defn list-of-files
  "
  Returns list of files from dir.

  Arguments:
  dirname -- dir containing files
  "
  [dirname]
  (.list (io/file dirname)))

(defn append-to-file
  "Uses spit to append to a file specified with its name as a string, or
   anything else that writer can take as an argument.  s is the string to
   append.
   http://clojuredocs.org/clojure_core/clojure.core/spit"
  [file-name s]
  (let [wrtr (io/writer file-name :append true)]
    (.write wrtr s)
    (.close wrtr)))

(defn read-as-csv [filename]
  (with-open [in-file (io/reader filename)]
    (doall
      (csv/read-csv in-file))))

(defn save-as-csv [data filename header-names]
  (with-open [f-out (io/writer filename)]
    (csv/write-csv f-out  [header-names])
    (csv/write-csv f-out data)))

(defn count-substring 
  "Use a sequence of regexp matches to count occurrences.
   Source http://www.rosettacode.org/wiki/Count_occurrences_of_a_substring#Clojure"
  [txt sub]
  (count (re-seq (re-pattern sub) txt)))

(defn transpose [m]
  (apply mapv vector m))

(defn filter-att [data-path att]
  (let [data (read-as-csv data-path)]
    (transpose (filter #(not= (first %) att) (transpose data)))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;farsec47.utils/filter-att</span>","value":"#'farsec47.utils/filter-att"}
;; <=

;; @@

;; @@
