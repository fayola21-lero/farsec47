;; gorilla-repl.fileformat = 1

;; @@
(ns farsec47.data
  (:use (incanter core stats))
  (:use (farsec47 tokenizing utils))
  (:require [farsec47.tokenizing :as t]
            [incanter.io :as iio]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Strip comments from chromium issues. ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def chromium-dir 
  (str user-home "/resources/data/downloaded/bugs-html/"))

(defn csv-col-size ;limit of csv column
  [s]
  (if (< (count s) 32700)
    s
    (subs s 0 32700)))

(defn scrub-html-page
  [url]
  (let [s     (fetch-page url)
        start (.indexOf s "Issue ")
        end   (.indexOf s "Comment ")]
    (if (or (= start -1) (= end -1)) 
      "" 
      (subs s start end))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Put chromium issues into .csv file. ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn label-report
  [string]
  (if (= -1 (.indexOf string "q=label:Security")) "0" "1"))

(defn extract-date
  [s]
  (if true 
    (re-find (re-matcher #"\w\w\w \d+, \d+" s)) ""))

(defn extract-ids
  [dir]
  (let [files   (rest (list-of-files dir))
        pattern (fn [s] (re-find (re-matcher #"\d+" s)))]
    (->> (map pattern files)
         (sort #(< (read-string %1) (read-string %2))))))

(defn strip-chromium-panel
  "Remove left panel text from html"
  [string]
  (let [start (.indexOf string "Status:")
        end   (.indexOf string "Sign")]
  	(if (or (= start -1) (= end -1)) 
   		string 
      	(str (subs string 0 start)
             (subs string end)))))

(defn strip-chromium-template
  "Remove template from html"
  [string]
  (let [start (.indexOf string "people")
        end   (.indexOf string "problem?")]
  	(if (or (= start -1) (= end -1)) 
   		string 
      	(str (subs string 0 start)
             (subs string end)))))

(defn strip-date
  [string]
  (if 
    (re-find (re-matcher #"\w\w\w \d+, \d+" string))
    (.replaceAll string (re-find (re-matcher #"\w\w\w \d+, \d+" string)) "") 
    string))

(defn strip-chromium-phrases
  "Remove template phrases."
  [string]
  (-> string
      (.replaceAll "What steps will reproduce the problem?" " ")
      (.replaceAll "people starred this issue and may be notified of changes." " ")
      (.replaceAll "What is the expected result?" " ")
      (.replaceAll "What happens instead?" " ")
      (.replaceAll "Please provide any additional information below." " ")
      (.replaceAll "Attach a screenshot if possible." " ")
      (.replaceAll "Back to list Sign in to add a comment" " ")
      (.replaceAll "Reported by" " ")
      (.replaceAll "Product Version" " ")
      (.replaceAll "Other browsers tested:" " ")
      (.replaceAll "Add OK or FAIL after other browsers where you have tested this issue" " ")
      ))
   
(defn save-chromium-csv ; to get security keywords
  [dir f]
  (let [ids (extract-ids dir)
        row (fn [id] 
              (let [report-html 
                    (scrub-html-page (str dir id ".html"))]
                [id
              	 (extract-date report-html) ;date
                 (-> report-html            ;report
                     (strip-chromium-panel)
                     (strip-html-tags)
                     (strip-chromium-template)
                     (strip-chromium-phrases)
                     (strip-date)
                     (csv-col-size)
                     (clojure.string/replace #"," " "))
                 (label-report report-html)]))] ;label
    (-> (filter #(not (empty? (nth % 2))) (map row ids))
        (save-as-csv f ["id" "date" "report" "security"]))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Load all data.                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def pattern (fn [s] (re-find (re-matcher #"\w+" s))))

(defmulti load-data identity)

(defn scrubbed-data 
  [key-data]
  (->> (load-data key-data)
       (transpose)
       (filter #(member? 
                  (pattern (first %)) 
                  ["issue_id" "description" 
                   "summary" "Security"]))
       (transpose)
       (rest)
       (map #(hash-map
               :id (read-string (nth % 0))
               :report   (clojure.string/trim 
                 		    (str (nth % 1) (nth % 2)))
               :security ;(read-string 
                           (clojure.string/trim (nth % 3))))
       (hash-map 
         :column-names [:id :report :security] 
         :rows)))

(defn scrubbed-chromium-data 
  [key-data]
  (->> (load-data key-data)
       (transpose)
       (filter #(member? 
                  (pattern (first %)) 
                  ["id" "report" "security"]))
       (transpose)
       (rest)
       (map #(hash-map
               :id (read-string (nth % 0))
               :report   (nth % 1)
               :security ;(read-string 
                           (clojure.string/trim (nth % 2))))
       (hash-map 
         :column-names [:id :report :security] 
         :rows)))


; Ambari
(defmethod load-data :ambari [_]
  (-> (str user-home "/resources/data/downloaded/Ambari.csv")
      (read-as-csv)))
(defmethod load-data :ambari-scrubbed [_]
  (scrubbed-data :ambari))

; Camel
(defmethod load-data :camel [_]
  (-> (str user-home "/resources/data/downloaded/Camel.csv")
      (read-as-csv)))
(defmethod load-data :camel-scrubbed [_]
  (scrubbed-data :camel))

; Derby
(defmethod load-data :derby [_]
  (-> (str user-home "/resources/data/downloaded/Derby.csv")
      (read-as-csv)))
(defmethod load-data :derby-scrubbed [_]
  (scrubbed-data :derby))

; Wicket
(defmethod load-data :wicket [_]
  (-> (str user-home "/resources/data/downloaded/Wicket.csv")
      (read-as-csv)))
(defmethod load-data :wicket-scrubbed [_]
  (scrubbed-data :wicket))

; Chromium
(defn save-data [] 
  (save-chromium-csv 
    chromium-dir 
    (str 
      user-home
      "/resources/data/scrubbed/Chromium.csv")))
(defmethod load-data :chromium-scrubbedxx [_]
  (-> (str user-home "/resources/data/scrubbed/Chromium.csv")
      (iio/read-dataset :delim \, :header true)))

(defmethod load-data :chromium-scrubbedx [_]
  (-> (str user-home "/resources/data/scrubbed/Chromium.csv")
      (read-as-csv)))
(defmethod load-data :chromium-scrubbed [_]
  (scrubbed-chromium-data :chromium-scrubbedx))







;(strip-html-tags (strip-chromium-panel (scrub-html-page (str user-home ;"/resources/data/downloaded/bugs-html/1246.html"))))
;(save-data)
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-string'>&quot;Issue 1246 : Printing individual frames &amp;lsaquo; Prev 13506 of 14142 Next &amp;rsaquo; 7 people starred this issue and may be notified of changes. Back to list Sign in to add a comment &amp;nbsp; Reported by tonyryan07 , Sep 04, 2008 Product Version : 0.2.149.27 (1583) URLs (if applicable) : Other browsers tested: Add OK or FAIL after other browsers where you have tested this issue: Safari 3: Firefox 3: IE 7: What steps will reproduce the problem? 1. Try to print an individual frame on a frames-enabled page 2. 3. What is the expected result? Be able to print individual frames on a frames-enabled page (probably applies to iframes as well). What happens instead? Not supported Please provide any additional information below. Attach a screenshot if possible. &quot;</span>","value":"\"Issue 1246 : Printing individual frames &lsaquo; Prev 13506 of 14142 Next &rsaquo; 7 people starred this issue and may be notified of changes. Back to list Sign in to add a comment &nbsp; Reported by tonyryan07 , Sep 04, 2008 Product Version : 0.2.149.27 (1583) URLs (if applicable) : Other browsers tested: Add OK or FAIL after other browsers where you have tested this issue: Safari 3: Firefox 3: IE 7: What steps will reproduce the problem? 1. Try to print an individual frame on a frames-enabled page 2. 3. What is the expected result? Be able to print individual frames on a frames-enabled page (probably applies to iframes as well). What happens instead? Not supported Please provide any additional information below. Attach a screenshot if possible. \""}
;; <=

;; @@
(defn find-replace
  [x]
  (if (or (= (get x :security) 0)
          (= (get x :security) "0"))
    (into x {:security "0"})
    (into x {:security "1"})))

(defn string-label
  []
(->> (load-data :chromium-scrubbed)
     (:rows)
     ;(folds 2 :id)
     ;(first)
     ;(:rows)
     ;(take 2)
     (map find-replace )
     (last)
     )
  )
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;farsec47.data/string-label</span>","value":"#'farsec47.data/string-label"}
;; <=

;; @@
(map str ["1" 2 3 4])
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-string'>&quot;1&quot;</span>","value":"\"1\""},{"type":"html","content":"<span class='clj-string'>&quot;2&quot;</span>","value":"\"2\""},{"type":"html","content":"<span class='clj-string'>&quot;3&quot;</span>","value":"\"3\""},{"type":"html","content":"<span class='clj-string'>&quot;4&quot;</span>","value":"\"4\""}],"value":"(\"1\" \"2\" \"3\" \"4\")"}
;; <=

;; @@
(->> (load-data :chromium-scrubbed)
     (:rows)
     (count))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-unkown'>41940</span>","value":"41940"}
;; <=

;; @@

;; @@
