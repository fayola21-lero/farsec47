;; gorilla-repl.fileformat = 1

;; @@
(ns farsec47.score-reports
  (:use (incanter core stats))
  (:use (farsec47 data utils))
  (:require [farsec47.tfidf :as tfidf]
            [farsec47.tokenizing :as t]
    		[incanter.core :as i]
            [incanter.stats :as stat]
            [incanter.io :as iio] 
            [clojure.set :as set]
            [clojure.java.io :as io]))

(defn term-freq [corpus term]
  (inc (count (filter #(contains? % term) corpus)))) 

(defn get-freqs-cache [corpus]
  (reduce #(assoc %1 %2 (term-freq corpus %2)) {}
          (tfidf/get-corpus-terms corpus)))

(defmulti score-word identity)
(declare score-word-sq score-word-two score-word-none)

(defmethod score-word :sq [_]
  score-word-sq)
(defmethod score-word :two [_]
  score-word-two)
(defmethod score-word :none [_]
  score-word-none)

(defn score-word-sq
  [good bad word] ; good = nsbr
  (let [g (get good word 0)
        b (Math/pow (get bad word 0) 2)
        nbad  (count bad)
        ngood (count good)
        score ;(if (and (= g 0) (= b 0)) 0.0
        		(/ (min 1 (/ b nbad))
                 (+ (min 1 (/ g ngood))
                    (min 1 (/ b nbad))))
        ;)
        ]
    (->> score
         (float)
         (min 0.99)
         (max 0.01))))

(defn score-word-two
  [good bad word] ; good = nsbr
  (let [g (get good word 0)
        b (* (get bad word 0) 2)
        nbad  (count bad)
        ngood (count good)
        score (/ (min 1 (/ b nbad))
                 (+ (min 1 (/ g ngood))
                    (min 1 (/ b nbad))))]
    (->> score
         (float)
         (min 0.99)
         (max 0.01))))

(defn score-word-none
  [good bad word] ; good = nsbr
  (let [g (get good word 0)
        b (get bad word 0)
        nbad  (count bad)
        ngood (count good)
        score (/ (min 1 (/ b nbad))
                 (+ (min 1 (/ g ngood))
                    (min 1 (/ b nbad))))]
    (->> score
         (float)
         (min 0.99)
         (max 0.01))))

(defn score-words 
  [key-data key-farsec top-words]
  (let [corpus
        (->> (load-data key-data)
       		 (:rows)
             (folds 2 :id)
       		 (first)
       		 (:rows))
        sbr-corpus
       	(filter #(or
                 	(= (:security %) "1")
                    (= (:security %) 1)) corpus)
        nsbr-corpus
       	(filter #(or
                 	(= (:security %) "0")
                    (= (:security %) 0)) corpus)
        scrub
        (fn [xs]
          (->> xs
               (map :report)
       		   (t/scrub-text)
       		   (map frequencies)))
        sbr-cache 
        (get-freqs-cache (scrub sbr-corpus))
        nsbr-cache 
        (get-freqs-cache (scrub nsbr-corpus))]
    (map #(vector % ((score-word key-farsec) nsbr-cache sbr-cache %)) 
                  top-words)))

(defn score-report
  [scored-words report]
  (loop [word report result []]
    (if (empty? word)
      (->> result
           (remove #(= nil %))
           (combine-scores))
      (recur
        (rest word)
        (conj result 
              (get scored-words (first word)))))))

(defn score-reports ;not using yet, used to check number of filtered reports in crosswords.clj
  [fnc key-data key-farsec top-words]
  (let [scored-words
        (->> (score-words key-data key-farsec top-words)
             (into {}))
        reports
        (->> (load-data key-data)
       		 (:rows)
             (folds 2 :id)
       		 fnc
       		 (:rows)
             (filter #(or
                        (= (:security %) "0")
                        (= (:security %) 0)))
             (map :report)
             (t/scrub-text))]
    (map #(score-report scored-words %) reports)))

(defn score-test-reports ; used to score test reports.
  [fnc key-data key-farsec top-words]
  (let [scored-words
        (->> (score-words key-data key-farsec top-words)
             (into {}))
        reports
        (->> (load-data key-data)
       		 (:rows)
             (folds 2 :id)
       		 fnc
       		 (:rows)
             (map :report)
             (t/scrub-text))]
    (map #(score-report scored-words %) reports)))

(defn score-test-reports-print ; used to score test reports.
  [fnc key-data key-farsec top-words]
  (let [scored-words
        (->> (score-words key-data key-farsec top-words) ; From train data.
             (into {}))
        reports
        (->> (load-data key-data)
       		 (:rows)
             (folds 2 :id)
       		 fnc
       		 (:rows)
             (map :report)
             (t/scrub-text))]
    (map #(hash-map 
            :report %
            :score (score-report scored-words %)) reports)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;farsec47.score-reports/score-test-reports-print</span>","value":"#'farsec47.score-reports/score-test-reports-print"}
;; <=

;; @@

;; @@
