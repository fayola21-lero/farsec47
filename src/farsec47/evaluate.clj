;; gorilla-repl.fileformat = 1

;; @@
(ns farsec47.evaluate
  (:use (seer loading-files filtering naive-bayes 
              random-forest logistic-regression ib-k 
              multilayer-perceptron cpdp))
  (:use [farsec47 utils])
  (:use (incanter core stats io))
  (require [clojure.java.io :as io]))

(declare cpdp-id)

;---------------prediction------------------
(defn run-results
  [src tar & {:keys [learner one-filter exp file-cpdp]
                :or {learner naive-bayes one-filter "" exp "" file-cpdp ""}}]  
  (let [result    (cpdp-id src tar learner)
        g-measure (get result :g)
        pd        (get result :pd)
        pf        (get result :pf)
        f         (get result :f)
        prec      (get result :prec)
        err       (get result :err)
        tn        (get result :tn)
        tp        (get result :tp)
        fn        (get result :fn)
        fp        (get result :fp)
        pattern0   #"\w+\.csv"
        pattern1   #"\w+\-\w+\.csv"
        pattern2   #"\w+\-\w+\-\w+\.csv"
        pattern3   #"\w+\-\w+\-\w+\-\w+\.csv"
        learn      (re-find (re-pattern #"\w+\_\w+") (str learner))]        
    (append-to-file 
      file-cpdp 
      (str (re-find pattern1 src)","(re-find (if (= exp "tpp") pattern2 pattern1) tar)","one-filter","learn","tn","tp","fn","fp","err","pd","pf","prec","f","g-measure "\n"))))

(defn cr [source-file target-file targetid-file learner]
  (let [source    (format-data source-file)
        target    (format-data target-file)
        targetid  (rest (first (transpose (read-as-csv targetid-file))))
        buildc    (learner source)
        cmatrix   (map #(vector (.classValue (.get target %)) (.classifyInstance buildc (.get target %))) (range (.numInstances target)))]
    (apply vector (map #(apply vector (flatten (vector %1 %2))) targetid cmatrix))))

;---------------new cpdp to remove id------------------

(defn format-data-id [filename]
  (let [data0  (load-csv filename)
        data  (filter-attributes data0 ["id"])
        cols0 (.numAttributes data)]    
    (doto (num-to-nom data cols0)
      (.setClassIndex (dec cols0)))))

(defn format-data-noid [filename]
  (let [data  (load-csv filename)
        ;data  (filter-attributes data0 ["id"])
        cols0 (.numAttributes data)]    
    (doto (num-to-nom data cols0) 
      (.setClassIndex (dec cols0)))))

(defn cpdp-id [source-file target-file learner]
  (let [source  (format-data-id source-file)
        target  (format-data-id target-file)
        buildc  (learner source)
        cmatrix (frequencies (map #(vector (.classValue (.get target %))
                                    (.classifyInstance buildc (.get target %)))
                              (range (.numInstances target))))
        tp      (get cmatrix [1.0 1.0] 0) 
        tn      (get cmatrix [0.0 0.0] 0) 
        fp      (get cmatrix [0.0 1.0] 0)
        fn      (get cmatrix [1.0 0.0] 0)
        err     (* 100.0 (- 1 (/ (+ tp tn) (+ tp tn fp fn))))
        pd      (if (zero? tp) 0 (* 100.0 (/ tp (+ tp fn))))
        pf      (if (zero? fp) 0 (* 100.0 (/ fp (+ fp tn))))
        prec    (if (zero? tp) 0 (* 100.0 (/ tp (+ tp fp))))
        f       (if (or (zero? pd) (zero? prec)) 0 (/ (* 2 pd prec) (+ pd prec)))
        g       (if (or (zero? pd) (= 100 pf)) 0 (/ (* 2 pd (- 100 pf)) (+ pd (- 100 pf))))]
    (zipmap [:tp :tn :fp :fn :err :pd :pf :prec :f :g] [tp tn fp fn err pd pf prec f g])))

; For MAP
(defn cpdp-map [source-file target-file learner]
  (let [source  (format-data-id source-file)
        target  (format-data-id target-file)
        buildc  (learner source)
        cmatrix (map #(vector (.classValue (.get target %))
                                    (.classifyInstance buildc (.get target %)))
                              (range (.numInstances target)))]
    cmatrix))


; For scores
(defn label-scores
  [threshold scores]
  (map #(if (< % threshold) 0.0 1.0) scores))

(defn naive-scores
  [want got]
  (let [cmatrix (->> (map #(vector %1 %2) want got)
                     (frequencies))
  		tp      (get cmatrix [1.0 1.0] 0) 
  		tn      (get cmatrix [0.0 0.0] 0) 
        fp      (get cmatrix [0.0 1.0] 0)
        fn      (get cmatrix [1.0 0.0] 0)
        err     (* 100.0 (- 1 (/ (+ tp tn) (+ tp tn fp fn))))
        pd      (if (zero? tp) 0 (* 100.0 (/ tp (+ tp fn))))
        pf      (if (zero? fp) 0 (* 100.0 (/ fp (+ fp tn))))
        prec    (if (zero? tp) 0 (* 100.0 (/ tp (+ tp fp))))
        f       (if (or (zero? pd) (zero? prec)) 0 (/ (* 2 pd prec) (+ pd prec)))
        g       (if (or (zero? pd) (= 100 pf)) 0 (/ (* 2 pd (- 100 pf)) (+ pd (- 100 pf))))]
    (zipmap [:tp :tn :fp :fn :err :pd :pf :prec :f :g] [tp tn fp fn err pd pf prec f g])))

(defn save-naive-scores
  [tar-name output-name result]
  (let [g-measure (get result :g)
        pd        (get result :pd)
        pf        (get result :pf)
        f         (get result :f)
        prec      (get result :prec)
        err       (get result :err)
        tn        (get result :tn)
        tp        (get result :tp)
        fn        (get result :fn)
        fp        (get result :fp)
        scorer    (get result :scorer)]
    (append-to-file 
      output-name 
      (str tar-name","scorer","tn","tp","fn","fp","err","pd","pf","prec","f","g-measure "\n"))))

;---------------mean average precision------------------
(defn mean-avg-precision [want val]
  (let [precision-at (fn [] 
                       (loop [w want i 1 result []]
                         (if (= i (inc (count w)))
                           result
                           (recur
                             w
                             (inc i)
                             (conj result 
                                   (* 100 
                                      (float (/ (reduce + (take i w)) 
                                                (count (take i w))))))))))
        mean-precision-at (fn [] 
                            (float 
                              (/ 
                                (reduce + (take val (precision-at))) 
                                val)))]
    (mean-precision-at)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;farsec47.evaluate/mean-avg-precision</span>","value":"#'farsec47.evaluate/mean-avg-precision"}
;; <=

;; @@

;; @@
