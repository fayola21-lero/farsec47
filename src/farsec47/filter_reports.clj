;; gorilla-repl.fileformat = 1

;; @@
(ns farsec47.filter-reports
  (:use (incanter core stats))
  (:use (farsec47 data utils))
  (:require [farsec47.tokenizing :as t]
            [farsec47.crosswords :as c]
            [farsec47.score-reports :as s]
    		[incanter.core :as i]
            [incanter.stats :as stat]
            [incanter.io :as iio] 
            [clj-ml.data :as d]
            [clojure.set :as set]
            [clojure.java.io :as io]
            [clj-time.local :as l]
            [clj-time.core :as dt]))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; @@
;----------Utils-----------

(defn one-report
  [hash-report top-words]
  (map #(get hash-report % 0) top-words))

(defn exp-reports
  "Accepts the key-word of a data-set such as :ambari-scrubbed, 
   a function, which is either first or second to indicate train or 
   test data respectively and top-words are the security related
   key-words from the SBRs.
  
   The output is a data matrix with the frequency of each top-word
   in each bug report. The first column is the id and the last is
   the label: 1 or 0 to indicate SBR or NSBR respectively.
  
   Example: 
  	
   (->> (c/security-words :ambari)
        (map first)
        (exp-reports :ambari-scrubbed first))
   
  "
  [key-data fnc top-words]
  (let [corpus 
        (->> (load-data key-data)
       		 (:rows))
        train
        (->> corpus
             (folds 2 :id)
             (fnc)
             (:rows))
        ids
        (->> train
             (map :id))
        reports
        (->> train
             (map :report)
       		 (map t/scrub-one)
       		 (map frequencies)
             )
        security-label
        (->> train
             (map :security)
             (map str))]
    (cons (flatten (vector '("id") top-words '("label")))
    	(map #(flatten (vector %1 (one-report %2 top-words) %3)) 
         	ids reports security-label))))

;----------Farsec Filter-----------

(defn farsec-filtered 
  [key-data key-farsec top-words] 
  "FARSEC filters in three ways, which affect the scores of the 
   reports and therefore the number of filtered reports. 
   key-farsec indicates which one is used.
  
   1) farsec (:none)   = treats SBRs and NSBRs equally.
   2) farsectwo (:two) = adds bias by multiplying the number of SBRs by 2. 
   3) farsecsq (:sq)   = adds bias by squaring the number of SBRs. 
  
   Example:
  
   (->> (c/security-words :ambari)
        (map first)
        (farsec-filtered :ambari-scrubbed :two))
   
  "
  (let [scored-words
        (->> (s/score-words key-data key-farsec top-words)
             (into {}))
        nsbr-corpus
        (->> (load-data key-data)
       		 (:rows)
             (folds 2 :id)
       		 (first)
       		 (:rows)
       		 (filter #(or
                        (= (:security %) "0")
                        (= (:security %) 0))))
        sbr-corpus
        (->> (load-data key-data)
       		 (:rows)
             (folds 2 :id)
       		 (first)
       		 (:rows)
       		 (filter #(or
                        (= (:security %) "1")
                        (= (:security %) 1))))
        sbr-ids
        (->> sbr-corpus
             (map :id))
        sbr-reports
        (->> sbr-corpus
             (map :report)
             (map t/scrub-one)
             (map frequencies)
             )
        sbr-labels
        (->> sbr-corpus
             (map :security))
        inner-filter
        (fn [x] (->> x 
                     (:report) 
                     (t/scrub-one)
                     ))
        third-filter
        (fn [x] (nth x 2))
        filtered-corpus 
        (->> nsbr-corpus
       		 (map #(vector (:id %)
                           (:security %)
                           (inner-filter %) 
                           (s/score-report 
                             scored-words 
                             (inner-filter %))))  
             (filter #(< (last %) 0.75)))
        filtered-ids
        (->> filtered-corpus
             (map first))
        filtered-reports
        (->> filtered-corpus
             (map third-filter)
             (map frequencies)
             )
        filtered-labels
        (->> filtered-corpus
             (map second))]
    (cons (flatten (vector '("id") top-words '("label")))
          (apply concat
            [
              (map #(flatten (vector %1 (one-report %2 top-words) %3)) 
         		sbr-ids sbr-reports sbr-labels)
          	  (map #(flatten (vector %1 (one-report %2 top-words) %3)) 
         		filtered-ids filtered-reports filtered-labels)
        	]))))

;----------CLNI Filter-----------

(defn noisy-report?
  [one n threshold xs] ;n=5
  (let [feature-vals (fn [x] (-> x (rest) (butlast)))
        nums
        (int (Math/sqrt (count xs)))
        val-dist1
        (fn [part-xs]
  		(->> part-xs
       		 (pmap #(vector (set [one %])
                                (stat/euclidean-distance 
                                  (feature-vals one) 
                                  (feature-vals %))))))
        parts
        (->> (partition-all nums xs)
             (pmap val-dist1)
             (apply concat))
        top-n 
  		(->> parts
       		 (sort-by second)
             (take (inc n))
             (rest))
        diff-one
        (->> top-n
        	 (map first)
             (map second)
             (filter #(not= (last one) (last %)))
             (count))]
    (if (>= (float (/ diff-one n)) threshold)
      true false)))
 
(defn clni-filtered
  "From the paper, Dealing with Noise in Defect Prediction,
   Closest List Noise Identification (CLNI), is adapted and
   compared with FARSEC filtering.
  
   CLNI uses top-nearest neighbors to determine if an 
   instance is noisy. Hence it very slow on larger data-sets
   like Chromium.
  
   time-period-hrs is used to stop the algorithm after a set
   number of hours.
  
   Example:
  
   (->> (c/security-words :ambari)
        (map first)
        (clni-filtered :ambari-scrubbed first 5 0.99 8))
   
  "
  [key-data fnc n ep time-period-hrs top-words]
  (let [header    
        (first (exp-reports key-data fnc top-words))
        data      
        (rest (exp-reports key-data fnc top-words))
        sbr-data 
        (filter #(or (= (last %) 1) (= (last %) "1")) data)
        nsbr-data 
        (filter #(or (= (last %) 0) (= (last %) "0")) data)
        threshold
        (float (/ (count sbr-data) (count data)))
        noisy
        (fn [new-nsbr new-data]
          (->> new-nsbr
       	  	   (filter #(= true 
            	           (noisy-report? % n threshold new-data)))
               (apply vector)))
        end-time 
        (-> (l/local-now)
            (dt/plus (dt/hours time-period-hrs)))] 
    (loop [prev [] 
           i 0
           curr (noisy nsbr-data data)]
      (if (or
            (= i 100)
            (dt/after? (l/local-now) end-time)
            (empty? curr)
            (>= 
          	  (/ (->> (set/intersection (set prev) (set curr))
              	      (count))
             	 (max (count prev) (count curr)))
          	 ep))
        (cons header
              (filter #(not (member? % curr)) data))
      	(recur
          curr
          (inc i)
          (let [new-nsbr 
                (filter #(not (member? % curr)) nsbr-data)
                new-data 
                (apply concat [sbr-data new-nsbr])
                new-noisy
                (noisy new-nsbr new-data)]
            (println i " " end-time)
            (->> (apply concat [curr new-noisy])
                 (apply vector))))))))


(defn clnix-filtered
  "To speed up the algorithm, select the 100 nearest nsbrs 
   to each sbr, and only use these as potential noisy nsbrs.  
  
   Example:
  
   (->> (c/security-words :ambari)
        (map first)
        (clni-filtered :ambari-scrubbed first 5 0.99 8))
   
  "
  [key-data fnc n ep time-period-hrs top-words]
  (let [feature-vals (fn [x] (-> x (rest) (butlast)))
        header    
        (first (exp-reports key-data fnc top-words))
        data      
        (rest (exp-reports key-data fnc top-words))
        sbr-data 
        (filter #(or (= (last %) 1) (= (last %) "1")) data)
        nsbr-data 
        (filter #(or (= (last %) 0) (= (last %) "0")) data)
        threshold
        (float (/ (count sbr-data) (count data)))
        initial-noisy-nsbrs ; sample of potential nsbrs
        (loop [sd sbr-data result []]
          (if (empty? sd)
            (distinct (apply concat result))
            (recur
              (rest sd)
              (conj result 
                    (->> (map #(vector 
                                 % 
                                 (stat/euclidean-distance
                                   (feature-vals (first sd))
                                   (feature-vals %)))
                              nsbr-data)
                         (sort-by last)
                         (take 100)
                         (map first))))))
        noisy
        (fn [new-nsbr new-data]
          (->> new-nsbr
       	  	   (filter #(= true 
            	           (noisy-report? % n threshold new-data)))
               (apply vector)))
        end-time 
        (-> (l/local-now)
            (dt/plus (dt/hours time-period-hrs)))] 
    (loop [prev [] 
           i 0
           curr (noisy initial-noisy-nsbrs data)]
      (if (or
            (= i 100)
            (dt/after? (l/local-now) end-time)
            (empty? curr)
            (>= 
          	  (/ (->> (set/intersection (set prev) (set curr))
              	      (count))
             	 (max (count prev) (count curr)))
          	 ep))
        (cons header
              (filter #(not (member? % curr)) data))
      	(recur
          curr
          (inc i)
          (let [new-nsbr 
                (filter #(not (member? % curr)) initial-noisy-nsbrs)
                new-data 
                (apply concat [sbr-data new-nsbr])
                new-noisy
                (noisy new-nsbr new-data)]
            (println i " " end-time)
            (->> (apply concat [curr new-noisy])
                 (apply vector))))))))

;----------Baseline Filter-----------

(defn contains-crosswords?
  [report cross-words]
  (let [intersect (count (set/intersection 
                           (set report) 
                           (set cross-words)))]
    (if (> intersect 0)
      true
      false)))

(defn baseline-filtered
  "Remove all NSBRs containing security cross words.
   
   Example
  
   (baseline-filtered :ambari :ambari-scrubbed :ambari-nsbr)
  
  "
  [key-data key-data-scrubbed key-data-nsbr]
  (let [security-words
        (c/security-words key-data)
        top-words
        (map first security-words)
        cross-words
        (->> (c/security-words key-data-nsbr)
             (filter #(not= (second %) 0))
             (map first))
        nsbr-corpus
        (->> (load-data key-data-scrubbed)
       		 (:rows)
             (folds 2 :id)
       		 (first)
       		 (:rows)
       		 (filter #(or
                        (= (:security %) "0")
                        (= (:security %) 0))))
        sbr-corpus
        (->> (load-data key-data-scrubbed)
       		 (:rows)
             (folds 2 :id)
       		 (first)
       		 (:rows)
       		 (filter #(or
                        (= (:security %) "1")
                        (= (:security %) 1))))
        sbr-ids
        (->> sbr-corpus
             (map :id))
        sbr-reports
        (->> sbr-corpus
             (map :report)
             (map t/scrub-one)
             (map frequencies)
             )
        sbr-labels
        (->> sbr-corpus
             (map :security))
        inner-filter
        (fn [x] (->> x 
                     (:report) 
                     (t/scrub-one)
                     ))
        third-filter
        (fn [x] (nth x 2))
        filtered-corpus 
        (->> nsbr-corpus
       		 (map #(vector (:id %)
                           (:security %)
                           (inner-filter %) 
                           (contains-crosswords? % cross-words)))
             (filter #(= (last %) true)))
        filtered-ids
        (->> filtered-corpus
             (map first))
        filtered-reports
        (->> filtered-corpus
             (map third-filter)
             (map frequencies)
             )
        filtered-labels
        (->> filtered-corpus
             (map second))]
    (cons (flatten (vector '("id") top-words '("label")))
          (apply concat
            [
              (map #(flatten (vector %1 (one-report %2 top-words) %3)) 
         		sbr-ids sbr-reports sbr-labels)
          	  (map #(flatten (vector %1 (one-report %2 top-words) %3)) 
         		filtered-ids filtered-reports filtered-labels)
        	]))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;farsec47.filter-reports/baseline-filtered</span>","value":"#'farsec47.filter-reports/baseline-filtered"}
;; <=

;; @@
(comment
(->> (c/security-words :derby)
     (map first)
     (clni-filtered :derby-scrubbed first 5 0.99 8)
     ;(first)
     (count)
     (time))
)
  
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; @@

;; @@
